
from sqlalchemy import Column, Integer, String
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker
import os
import pyexcel
import argparse
 
Base = declarative_base()
 
class Person(Base):
    __tablename__ = 'person'
    id = Column(Integer, primary_key=True)
    name = Column(String(250), nullable=False)
    age = Column(Integer, nullable=False)

engine = create_engine('sqlite:///person.db')
Base.metadata.create_all(engine)
DBSession = sessionmaker(bind=engine)


def main(args=None):

    parser = argparse.ArgumentParser()
    parser.add_argument("file")
    args = parser.parse_args()

    if not os.path.exists(args.file):
        print("File does not exist")
    else:
        try:
            sheet = pyexcel.get_sheet(file_name=args.file)
            session = DBSession()
            pyexcel.save_as(file_name=file, name_columns_by_row=0, dest_session=session, dest_table=Person)
            processed_count = sheet.number_of_rows() - 1
            print("The number of rows processed is {}".format(processed_count))
            # we need to decrement row count because first row is the header row
        except Exception as e:
            print(str(e))


if __name__ == "__main__":
    main()