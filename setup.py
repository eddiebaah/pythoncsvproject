import sys
from setuptools import setup

setup(name='savecsv',
      version='0.1.0',
      packages=['savecsv'],
      entry_points={
          'console_scripts': [
              'savecsv = savecsv.cli.__main__:main'
          ]
      },
    )